//http://buildnewgames.com/webgl-threejs/
//https://aerotwist.com/tutorials/getting-started-with-three-js/
//http://code.tutsplus.com/tutorials/webgl-with-threejs-basics--net-35688

//create a simple scene with a cube and a light

var WIDTH = window.innerWidth;
var HEIGHT = window.innerHeight;
var VIEW_ANGLE = 50;
var ASPECT = WIDTH / HEIGHT;
var NEAR = 0.1;
var FAR = 10000;
var scene;
var renderer;
var camera;
var light;

var socket = io();

function sendMsg(){
	socket.emit('chat message', document.getElementById('m').value);
	document.getElementById('m').innerHTML ="";
	return false;
}

socket.on('chat message', function(msg){
	document.getElementById("messages").innerHTML += msg;
  });

function setup(){
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
	renderer = new THREE.WebGLRenderer();
	
	renderer.setSize(WIDTH, HEIGHT);
	document.body.appendChild( renderer.domElement );

	var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	var material = new THREE.MeshLambertMaterial( { color: 0x00ff00 } );
	var cube = new THREE.Mesh( geometry, material );
	scene.add(cube);

	camera.position.z = 5;
	
	// create a point light
	light =
	  new THREE.PointLight(0xFFFFFF);
	
	// set its position
	light.position.x = 10;
	light.position.y = 50;
	light.position.z = 130;
	
	// add to the scene
	scene.add(light);
	
	draw();	
}

function draw(){
	renderer.render(scene, camera);
	requestAnimationFrame(draw);
}